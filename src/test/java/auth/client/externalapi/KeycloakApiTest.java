package auth.client.externalapi;

import auth.client.payload.response.TokenResponse;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import static auth.client.util.TestResponse.TOKEN_RESPONSE;
import static org.junit.jupiter.api.Assertions.assertEquals;

class KeycloakApiTest {

  private MockWebServer mockWebServer;
  private KeycloakApi keycloakApi;
  private String url = "";

  @BeforeEach
  void setUp() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start();
    keycloakApi = new KeycloakApi(WebClient.builder().build());
    url = String.format("http://localhost:%s", mockWebServer.getPort());
  }

  @AfterEach
  void tearDown() throws IOException {
    mockWebServer.shutdown();
  }

  @Test
  void getClientTokenTest() {
    mockWebServer.enqueue(
        new MockResponse().setResponseCode(200)
            .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .setBody(TOKEN_RESPONSE)
    );

    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
    bodyValues.add("grant_type", "");
    bodyValues.add("refresh_token", "");

    Mono<TokenResponse> tokenResponse = keycloakApi.getUserToken(bodyValues,"","", url);

    StepVerifier
        .create(tokenResponse)
        .assertNext(token -> {
          assertEquals("access_token", token.accessToken());
          assertEquals("refresh_token", token.refreshToken());
        })
        .verifyComplete();
  }
}