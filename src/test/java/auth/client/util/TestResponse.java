package auth.client.util;

public class TestResponse {
  public static String TOKEN_RESPONSE = """
      {
      	"access_token": "access_token",
      	"expires_in": 900,
      	"refresh_expires_in": 1800,
      	"refresh_token": "refresh_token",
      	"token_type": "bearer",
      	"not-before-policy": 0,
      	"session_state": "32d07c84-5ff3-4bc7-b8d0-328977dc5cac",
      	"scope": ""
      }
      """;
}
