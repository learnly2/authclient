package auth.client.util;

import okhttp3.mockwebserver.MockWebServer;

import java.io.IOException;

public class MockServerUtil {
  private MockWebServer mockWebServer;

  public MockWebServer mockWebServer() {
    return new MockWebServer();
  }

  public void startServer() throws IOException {
    if (mockWebServer == null) {
      mockWebServer();
    }

    mockWebServer.start();
  }

  public void stopServer() throws IOException {
    if (mockWebServer != null) {
      mockWebServer.shutdown();
    }
  }
}
