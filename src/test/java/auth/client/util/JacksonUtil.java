package auth.client.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import reactor.core.publisher.Mono;

public class JacksonUtil {
  private ObjectMapper mapper = new ObjectMapper();

  public <T> Mono<T> deserializeObject(String json, TypeReference<T> typeReference) {
    try {
      return Mono.just(mapper.readValue(json, typeReference));
    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize object!", e);
    }
  }
}
