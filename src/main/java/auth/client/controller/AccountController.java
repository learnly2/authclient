package auth.client.controller;

import auth.client.payload.request.AdminTokenRequest;
import auth.client.payload.request.LogOutRequest;
import auth.client.payload.request.RefreshTokenRequest;
import auth.client.payload.response.TokenResponse;
import auth.client.util.KeycloakUtil;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/api/v1/account", produces = {"application/json; charset=utf-8"}, consumes = {"application/json"})
public class AccountController {

    private final KeycloakUtil keyCloakUtil;

    public AccountController(KeycloakUtil keyCloakUtil) {
        this.keyCloakUtil = keyCloakUtil;

    }

    @Operation(summary = "Request admin oauth2 token from keycloak")
    @PostMapping(value = "/admin/token")
    public Mono<TokenResponse> adminToken(@Valid @RequestBody AdminTokenRequest request) {
        return keyCloakUtil.getUserToken(request.username(), request.password());
    }

    @Operation(summary = "Token refresh")
    @PostMapping(value = "/token/refresh")
    public Mono<TokenResponse> refreshToken(@Valid @RequestBody RefreshTokenRequest request) {
        return keyCloakUtil.refreshUserToken(request.refreshToken());
    }

    @Operation(summary = "Logout user")
    @PostMapping(value = "/logout")
    @ResponseStatus(HttpStatus.CONTINUE)
    public Mono<ResponseEntity<Void>> logoutUser(@Valid @RequestBody LogOutRequest request) {
        return keyCloakUtil.logout(request.refreshToken());
    }
}
