package auth.client.externalapi;

import auth.client.exception.BadRequestException;
import auth.client.exception.IntegrationException;
import auth.client.exception.UnauthorizedException;
import auth.client.payload.response.TokenResponse;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class KeycloakApi {
    private final WebClient webClient;

    public KeycloakApi(WebClient webClient) {
        this.webClient = webClient;
    }

    public Mono<TokenResponse> getUserToken(
            MultiValueMap<String, String> bodyValues,
            String clientId,
            String secret,
            String url
    ) {
        try {

            return webClient.post()
                    .uri(new URI(url.strip()))
                    .headers(httpHeaders -> httpHeaders.setBasicAuth(Base64.getEncoder().encodeToString((clientId + ":" + secret).getBytes(UTF_8))))
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .accept(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromFormData(bodyValues))
                    .retrieve()
                    .onStatus(status -> status.value() == 401, response -> response.bodyToMono(String.class).map(UnauthorizedException::new))
                    .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
                    .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
                    .bodyToMono(TokenResponse.class);
        } catch (Exception e) {
            throw new IntegrationException(e.getMessage(), e);
        }
    }


    public Mono<ResponseEntity<Void>> logOutUser(
            MultiValueMap<String, String> bodyValues,
            String clientId,
            String secret,
            String url
    ) {
        try {
            return webClient
                    .post()
                    .uri(url.strip())
                    .headers(httpHeaders -> httpHeaders.setBasicAuth(Base64.getEncoder().encodeToString((clientId + ":" + secret).getBytes(UTF_8))))
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .accept(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromFormData(bodyValues))
                    .retrieve()
                    .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
                    .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
                    .toBodilessEntity();
        } catch (Exception e) {
            throw new IntegrationException(e.getMessage(), e);
        }
    }
}
