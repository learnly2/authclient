package auth.client.util;

import auth.client.externalapi.KeycloakApi;
import auth.client.payload.response.TokenResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

@Component
public class KeycloakUtil {

  @Value("${keycloak.client_id}")
  private String clientId;

  @Value("${keycloak.client_secret}")
  private String clientSecret;

  @Value("${keycloak.user.grant_type}")
  private String userGrantType;

  @Value("${keycloak.refresh_token.grant_type}")
  private String refreshTokenGrantType;

  @Value("${keycloak.token.url}")
  private String keycloakTokenUrl;

  @Value("${keycloak.logout.url}")
  private String keycloakLogoutUrl;

  private final KeycloakApi keycloakApi;

  public KeycloakUtil(KeycloakApi keycloakApi) {
    this.keycloakApi = keycloakApi;
  }

  public Mono<TokenResponse> getUserToken(String username, String password) {
    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
    bodyValues.add("grant_type", userGrantType);
    bodyValues.add("username", username);
    bodyValues.add("password", password);
    return keycloakApi.getUserToken(bodyValues, clientId, clientSecret, keycloakTokenUrl);
  }

  public Mono<TokenResponse> refreshUserToken(String refreshToken) {
    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
    bodyValues.add("grant_type", refreshTokenGrantType);
    bodyValues.add("refresh_token", refreshToken);
    return keycloakApi.getUserToken(bodyValues, clientId, clientSecret, keycloakTokenUrl);
  }

  /**
   *
   * @param refreshToken
   * @return
   */
  public Mono<ResponseEntity<Void>> logout(String refreshToken) {
    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
    bodyValues.add("refresh_token", refreshToken);
    return keycloakApi
            .logOutUser(bodyValues, clientId, clientSecret, keycloakLogoutUrl);
  }
}


