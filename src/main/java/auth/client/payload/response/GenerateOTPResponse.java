package auth.client.payload.response;

public record GenerateOTPResponse(boolean status) {
}
