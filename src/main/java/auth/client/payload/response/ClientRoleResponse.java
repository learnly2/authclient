package auth.client.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public record ClientRoleResponse(
    String id,
    String name,
    String description,
    boolean composite,
    boolean clientRole,
    String containerId
) {
}
