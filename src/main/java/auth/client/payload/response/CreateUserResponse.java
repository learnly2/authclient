package auth.client.payload.response;

public record CreateUserResponse(String userId) {
}
