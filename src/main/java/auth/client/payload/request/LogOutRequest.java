package auth.client.payload.request;

import jakarta.validation.constraints.NotBlank;

public record LogOutRequest(@NotBlank(message = "Refresh token is required") String refreshToken) {
}
