package auth.client.payload.request;

import jakarta.validation.constraints.NotBlank;

public record VerifyOTPRequest(
        @NotBlank(message = "User ID is required") String userId,
        @NotBlank(message = "OTP token is required") String otp,
        @NotBlank(message = "Verification type ID is required") String verificationTypeId
) {
}
