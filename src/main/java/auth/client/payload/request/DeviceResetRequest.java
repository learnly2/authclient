package auth.client.payload.request;

import jakarta.validation.constraints.NotBlank;

public record DeviceResetRequest(
        @NotBlank(message = "OTP token is required") String otp,
        @NotBlank(message = "Security question answer is required") String answer,
        @NotBlank(message = "Device ID is required") String deviceId
) {
}
