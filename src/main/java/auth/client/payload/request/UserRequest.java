package auth.client.payload.request;

import java.util.List;

public record UserRequest(
    String username,
    boolean enabled,
    boolean emailVerified,
    List<UserCredentialRequest> credentials,
    List<String> groups
) {
}
