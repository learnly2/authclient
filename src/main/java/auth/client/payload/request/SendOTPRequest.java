package auth.client.payload.request;

import jakarta.validation.constraints.NotBlank;

public record SendOTPRequest(@NotBlank(message = "Device identity is required") String deviceId) {
}
