package auth.client.payload.request;

import jakarta.validation.constraints.NotBlank;

public record ClientRegistrationRequest(
        @NotBlank(message = "Name is required") String name,
        @NotBlank(message = "Phone number is required") String phoneNumber,
        @NotBlank(message = "Pin is required") String pin,
        @NotBlank(message = "Country reference is required") String countryId,
        @NotBlank(message = "Client category reference is required") String clientCategoryId,
        @NotBlank(message = "Device ID is required") String deviceId
) {
}
