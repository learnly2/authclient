package auth.client.payload.request;

import jakarta.validation.constraints.NotBlank;

public record DriverRegistrationRequest(
    @NotBlank(message = "Name is required") String name,
    @NotBlank(message = "Date of birth is required") String dob,
    @NotBlank(message = "Phone number is required") String phoneNumber,
    @NotBlank(message = "Pin is required") String pin,
    @NotBlank(message = "Country reference is required") String countryId,
    @NotBlank(message = "Device ID is required") String deviceId,
    @NotBlank(message = "Security Question ID is required") String securityQuestionId,
    @NotBlank(message = "Security Question answer is required") String answer
) {
}
