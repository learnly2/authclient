package auth.client.payload.request;

public record UserCredentialRequest(String type, String value, boolean temporary) {
}
