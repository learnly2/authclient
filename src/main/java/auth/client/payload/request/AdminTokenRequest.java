package auth.client.payload.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record AdminTokenRequest(
        @NotBlank(message = "Username is required") @Email(message = "Not a valid email") String username,
        @NotBlank(message = "Password is required") String password
) {
}
