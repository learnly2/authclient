package auth.client.exception;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ExceptionAdvice {

  @ExceptionHandler(WebExchangeBindException.class)
  public ResponseEntity<Mono<ExceptionResponse>> handleException(WebExchangeBindException e) {
    var errors = e.getBindingResult()
        .getAllErrors()
        .stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage)
        .collect(Collectors.toList());
    return ResponseEntity.badRequest().body(Mono.just(new ExceptionResponse(errors)));
  }

  @ExceptionHandler(IntegrationException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(IntegrationException e) {
    return ResponseEntity
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }

  @ExceptionHandler(UnauthorizedException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(UnauthorizedException e) {
    return ResponseEntity
        .status(HttpStatus.UNAUTHORIZED)
        .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(BadRequestException e) {
    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }

  @ExceptionHandler(DeviceIdentityException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(DeviceIdentityException e) {
    return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }
}
