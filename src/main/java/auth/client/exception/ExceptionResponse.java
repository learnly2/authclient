package auth.client.exception;

import java.util.List;

public record ExceptionResponse (List<String> errors){
}
