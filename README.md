# Spring Boot Application - README

This README file provides instructions on how to run a ``authClient`` stored in GitLab. Follow the steps below to get started.
This service integrates with keycloak and provide access to keycloak authentication services.

## Prerequisites

Before running the application, ensure that you have the following installed:

- Java Development Kit (JDK) 17 LTS or higher
- Maven build tool
- Git

## Clone the Repository

1. Open a terminal or command prompt.
2. Clone the `https://gitlab.com/learnly2/authclient.git` repository using the `git clone` command:


## Build the Application

1. Navigate to the project directory:
2. Build the application using Maven and execute the following command `mvn -Dspring.profiles.active=dev package`


This command compiles the source code, runs tests, and creates an executable JAR file.

## Run the Application

1. After the build process completes successfully, navigate to target directory `cd target`
2. Run the following command `java -Dspring.profiles.active=dev -jar auth-client.jar` to run the app
3. The application will start, and you should see logs indicating its progress. The `authClient` will run on port `5001` so make sure no other process is running on the same port

**Note** In case you need to `Dockerize` the app run the following command `docker build --build-arg APP_ENV="dev" --tag="ph-auth-client" .` from the project root directory.

## Access the Application

Once the application is running, you can access it using a web browser or an API testing tool. By default, the application runs on `http://localhost:5001`.
You can also access the Swagger API documentation on `http://localhost:5001/swagger/webjars/swagger-ui/index.html#/`

## Configuration
This service does not require access to database.
For authentication purpose, this service require access to running and configured [keycloak identity service](https://www.keycloak.org/). The instructions on how to configure and install keycloak is provided in `tools repository` folder `README.md`.
In `dev / local` environment, These are the endpoints & configurations required by keycloak
```
###client
keycloak.client_id=pharmacypoc
keycloak.client_secret=zCFQC4Gvf2jXxUk8bpUvv5i1ZhFMtrLZ

###user
keycloak.user.grant_type=password
keycloak.refresh_token.grant_type=refresh_token

###token url
keycloak.token.url=http://0.0.0.0:8080/realms/pharmacy/protocol/openid-connect/token

###logout url
keycloak.logout.url=http://0.0.0.0:8080/realms/pharmacy/protocol/openid-connect/logout
```
If there's need to modify keycloak configuration, modify the `application-dev.properties`  file located in the project's resources directory. These keycloak configurations & endpoints must be changed to reflect what you have in your environment.

**Note** It's important to maintain the port number as `5001` as required by the `gatewayService` for routing purpose.

## Conclusion

You have successfully cloned, built, and run the Spring Boot application stored in GitLab. In case of any challenge please reach out.
